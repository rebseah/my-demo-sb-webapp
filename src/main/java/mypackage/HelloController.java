package mypackage;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    //CRUD = Create, Read, Update, Delete

    // each method can be invoked
    // @RequestMapping
    // never invoke method by method name only URL
    // http://localhost:8080/hello
    @GetMapping ("/hello")
    public String getHelloMessage() {
        return "Hello World";}

    }
